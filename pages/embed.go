package pages

import (
	"strings"

	"codeberg.org/rimgo/rimgo/api"
	"codeberg.org/rimgo/rimgo/utils"
	"github.com/gofiber/fiber/v2"
)

func HandleEmbed(c *fiber.Ctx) error {
	utils.SetHeaders(c)
	c.Set("Cache-Control", "public,max-age=31557600")
	c.Set("Content-Security-Policy", "default-src 'none'; base-uri 'none'; form-action 'none'; media-src 'self' https://i.imgur.com https://i.stack.imgur.com; style-src 'self'; img-src 'self' https://i.imgur.com https://i.stack.imgur.com; block-all-mixed-content")

	post, err := api.Album{}, error(nil)
	switch {
	case strings.HasPrefix(c.Path(), "/a"):
		post, err = ApiClient.FetchAlbum(c.Params("postID"))
	case strings.HasPrefix(c.Path(), "/gallery"):
		post, err = ApiClient.FetchPosts(c.Params("postID"))
	default:
		post, err = ApiClient.FetchMedia(c.Params("postID"))
	}
	if err != nil && err.Error() == "ratelimited by imgur" {
		return utils.RenderError(c, 429)
	}
	if err != nil && post.Id == "" && strings.Contains(err.Error(), "404") {
		return utils.RenderError(c, 404)
	}
 	if err != nil {
		return err
	}

	return c.Render("embed", fiber.Map{
		"post": post,
	})
}

func HandleGifv(c *fiber.Ctx) error {
	utils.SetHeaders(c)
	c.Set("Cache-Control", "public,max-age=31557600")
	c.Set("Content-Security-Policy", "default-src 'none'; base-uri 'none'; form-action 'none'; media-src 'self'; style-src 'self'; img-src 'self'; block-all-mixed-content")

	return c.Render("gifv", fiber.Map{
		"id": c.Params("postID"),
	})
}
