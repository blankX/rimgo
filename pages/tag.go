package pages

import (
	"strconv"

	"codeberg.org/rimgo/rimgo/utils"
	"github.com/gofiber/fiber/v2"
)

func HandleTag(c *fiber.Ctx) error {
	utils.SetHeaders(c)
	c.Set("X-Frame-Options", "DENY")
	c.Set("Cache-Control", "public,max-age=604800")
	c.Set("Content-Security-Policy", "default-src 'none'; frame-ancestors 'none'; base-uri 'none'; form-action 'self'; style-src 'unsafe-inline' 'self'; media-src 'self' https://i.imgur.com https://i.stack.imgur.com; img-src 'self' https://i.imgur.com https://i.stack.imgur.com; manifest-src 'self'; block-all-mixed-content")

	page := "1"
	if c.Query("page") != "" {
		page = c.Query("page")
	}

	pageNumber, err := strconv.Atoi(c.Query("page"))
	if err != nil {
		pageNumber = 0
	}

	tag, err := ApiClient.FetchTag(c.Params("tag"), c.Query("sort"), page)
	if err != nil && err.Error() == "ratelimited by imgur" {
		return utils.RenderError(c, 429)
	}
	if err != nil {
		return err
	}
	if tag.Display == "" {
		return utils.RenderError(c, 404)
	}

	return c.Render("tag", fiber.Map{
		"tag":      tag,
		"page":     page,
		"nextPage": pageNumber + 1,
		"prevPage": pageNumber - 1,
	})
}
