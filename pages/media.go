package pages

import (
	"net/http"
	"os"
	"strings"

	"codeberg.org/rimgo/rimgo/utils"
	"github.com/gofiber/fiber/v2"
)

func HandleMedia(c *fiber.Ctx) error {
	c.Set("Cache-Control", "public,max-age=31557600")
	c.Set("Content-Security-Policy", "default-src 'none'; style-src 'self'; img-src 'self'")
	if strings.HasPrefix(c.Path(), "/stack") {
		return handleMedia(c, "https://i.stack.imgur.com/"+strings.ReplaceAll(c.Params("baseName"), "stack/", "")+"."+c.Params("extension"))
	} else {
		return handleMedia(c, "https://i.imgur.com/"+c.Params("baseName")+"."+c.Params("extension"))
	}
}

func HandleUserCover(c *fiber.Ctx) error {
	c.Set("Cache-Control", "public,max-age=604800")
	c.Set("Content-Security-Policy", "default-src 'none'")
	return handleMedia(c, "https://imgur.com/user/"+c.Params("userID")+"/cover?maxwidth=2560")
}

func HandleUserAvatar(c *fiber.Ctx) error {
	c.Set("Cache-Control", "public,max-age=604800")
	c.Set("Content-Security-Policy", "default-src 'none'")
	return handleMedia(c, "https://imgur.com/user/"+c.Params("userID")+"/avatar")
}

func handleMedia(c *fiber.Ctx, url string) error {
	utils.SetHeaders(c)

	if os.Getenv("FORCE_WEBP") == "1" && c.Query("no_webp") == "" && c.Accepts("image/webp") == "image/webp" && !strings.HasPrefix(c.Path(), "/stack") {
		url = strings.ReplaceAll(url, ".png", ".webp")
		url = strings.ReplaceAll(url, ".jpg", ".webp")
		url = strings.ReplaceAll(url, ".jpeg", ".webp")
	}

	if strings.HasPrefix(c.Path(), "/stack") && strings.Contains(c.OriginalURL(), "?") {
		url = url + "?" + strings.Split(c.OriginalURL(), "?")[1]
	}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return err
	}

	utils.SetReqHeaders(req)

	if c.Get("Range") != "" {
		req.Header.Set("Range", c.Get("Range"))
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	if res.StatusCode == 404 || strings.Contains(res.Request.URL.String(), "error/404") {
		return utils.RenderError(c, 404)
	} else if res.StatusCode == 429 {
		return utils.RenderError(c, 429)
	}

	c.Set("Accept-Ranges", "bytes")
	c.Set("Content-Type", res.Header.Get("Content-Type"))
	c.Set("Content-Length", res.Header.Get("Content-Length"))
	if res.Header.Get("Content-Range") != "" {
		c.Set("Content-Range", res.Header.Get("Content-Range"))
	}

	return c.SendStream(res.Body)
}
