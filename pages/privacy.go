package pages

import (
	"github.com/gofiber/fiber/v2"

	"codeberg.org/rimgo/rimgo/utils"
)

func HandlePrivacy(c *fiber.Ctx) error {
	utils.SetHeaders(c)
	c.Set("X-Frame-Options", "DENY")
	c.Set("Content-Security-Policy", "default-src 'none'; form-action 'self'; style-src 'self'; img-src 'self'; manifest-src 'self'; block-all-mixed-content")

	return c.Render("privacy", fiber.Map{
		"config":  utils.Config,
		"version": VersionInfo,
	})
}
