package utils

import (
	"strconv"
	"strings"

	"codeberg.org/rimgo/rimgo/static"
	"github.com/gofiber/fiber/v2"
)

func RenderError(c *fiber.Ctx, code int) error {
	if !strings.Contains(c.Get("Accept"), "html") && c.Params("extension") != "" {
		codeStr := "generic"
		if code != 0 {
			codeStr = strconv.Itoa(code)
		}
		img, _ := static.GetFiles().ReadFile("img/error-" + codeStr + ".png")
		c.Set("Content-Type", "image/png")
		return c.Status(code).Send(img)
	} else {
		return c.Status(code).Render("errors/" + strconv.Itoa(code), fiber.Map{
			"path": c.Path(),
		})
	}
}