module codeberg.org/rimgo/rimgo

go 1.21

toolchain go1.21.4

require (
	github.com/PuerkitoBio/goquery v1.8.1
	github.com/aymerick/raymond v2.0.2+incompatible
	github.com/dustin/go-humanize v1.0.1
	github.com/gofiber/fiber/v2 v2.52.0
	github.com/gofiber/template/handlebars/v2 v2.1.7
	github.com/joho/godotenv v1.5.1
	github.com/microcosm-cc/bluemonday v1.0.26
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/tidwall/gjson v1.17.0
	gitlab.com/golang-commonmark/linkify v0.0.0-20200225224916-64bca66f6ad3
)

require (
	github.com/andybalholm/brotli v1.1.0 // indirect
	github.com/andybalholm/cascadia v1.3.2 // indirect
	github.com/aymerick/douceur v0.2.0 // indirect
	github.com/gofiber/template v1.8.2 // indirect
	github.com/gofiber/utils v1.1.0 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/gorilla/css v1.0.1 // indirect
	github.com/klauspost/compress v1.17.6 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mattn/go-runewidth v0.0.15 // indirect
	github.com/philhofer/fwd v1.1.2 // indirect
	github.com/rivo/uniseg v0.4.6 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.1 // indirect
	github.com/tinylib/msgp v1.1.9 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.51.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/net v0.20.0 // indirect
	golang.org/x/sys v0.16.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
